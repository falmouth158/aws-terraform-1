terraform {
  backend "s3" {
    bucket  = "aws-terraform-1-state-tf"
    key     = "aws-terraform-1.tfstate"
    region  = "us-east-1"
    encrypt = true
  }
  required_version = "~> v1.1.0"
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.0.0"
    }
    null = {
      source  = "hashicorp/null"
      version = "~> 3.0.0"
    }
  }
}
provider "aws" {
  region  = "us-east-1"
  profile = "default"
}
locals {
  prefix = "${var.prefix}-${terraform.workspace}"
  common_tags = {
    Environment = terraform.workspace
    Project     = var.project
    Owner       = var.contact
    ManagedBy   = "Terraform"
  }
}
data "aws_region" "current" {}

