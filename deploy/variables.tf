variable "prefix" {
  default = "aws-terra1"
}
variable "project" {
  default = "aws-terraform-1"
}
variable "contact" {
  default = "email@maintainer.com"
}
